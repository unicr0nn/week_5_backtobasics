#include <Arduino.h> // Library behúzása a projectbe

#define LED_PIN_1 12 // Preprocesszor constans létrehozása, nincs '=' jel
                     // az érték és a neve között, és nincs ';' a végén

uint16_t globalVar = 0; // Minden függvény számára látható globális változó,
                        // egyszerre létrehozva ls értékadással.

const double PI = 3.141592653589793238; // Ez egy globális Constans válto, azaz nem lehet változtatni az értékét.
                                        // (Igazából lehet egy mocskos trükkel, de olyat nem csinálunk :D )
                                        // hasonló egy definehoz, de ennek van típusa a Defineval szemben, ami olyan mintha csak éppeb akkor
                                        // gépelnénk le, amikor meghívjuk. Találkozhattok azzal hogy szokás a Pineknek így kezdőértéket adni, pl:
                                        // const uint8_t ledPin = 10;

int myFunc(uint16_t input); // Egy függvény prototype, itt figyelmeztetjük a fordítót, hogy
                            // valahol egy függvény deklarálva van, és ne rezeljen be, hogyha
                            // elöbb találkozik egy függvényhívással mint a deklarációval. 
                            // (Doksi alján van deklarálva)

bool isGreater(int a, int b) {  // Függvény deklaráció, megeszi az 'a' és a 'b' argumenteket
    if(a > b) {                 // amik integer típusú számok, és ha  'a' nagyobb mint 'b' akkor
        return true;            // igazzal '1' tér vissza ha hamis akkor hamissal '0'
    } else {
        return false;
    }
}

/* Következő szekció az adott műveletekhez kellő Defineok, és global változók */
/* ANALOG BEOLVASÁSHOZ */
uint16_t analogValue1 = 0;  // Ebbe fog bemenni az analóg érték
#define POTMETER_1 2        // Ezen a pinen mérünk

/* DIGITAL BEOLVASÁSHOZ */
#define BUTTON_1 4

/* PWM JEL GENERÁLÁSHOZ */
#define LED_PWM_1 10
const uint8_t pwmChannel_1  = 0; // 0-tól 15-ig terjedhet Azaz 16db PWM csatornát lehet
                                 // külön külön felkonfigurálni
const double frequency_1 = 5000; // A PWM jelünk frekvenciája Hz-ben
const uint8_t resolution = 8;    // A felbontása a pwmnek, ez 1 - 16 ig mehet, Bitben kell érteni
                                 // Pl a 4 bites felbontás 2^4 = 16 szeletre oszt egy PWM ciklus.
                                 // Esetünkben 0 - 255

// BUTTON INTERRUPT //
#define BUTTON_INTERRUPT_1 4
void interruptFunc_1() {                            // Intterupt kezelő fuggvény
    Serial.print("led state: ");
    if(digitalRead(BUTTON_INTERRUPT_1) == HIGH) {
        Serial.println("HIGH");
    } else {
        Serial.println("LOW");
    }
}

void setup() {      
    // Miután az összes függvényeken kívüli global változókat létrehozta a program, ez a
    // függvény fog elsőként lefutni. Ide kell raknunk a rendszerünknek az inicializálásához
    // szükséges beállításainkat. Nézzük az alapokat amiket tanultunk:

    pinMode(LED_PIN_1, OUTPUT);     // LED_1 előre definiált pint kimenetre állítja be.
    
    pinMode(BUTTON_1, INPUT);   // BUTTON_1 előre definiált pint bemeneetre állítja be.
    
    Serial.begin(9600)          // Elindítja a Serial UART kommunikációs portot. 9600 bit / sec sebességen

    // Intterupt összeköttetés
    attachInterrupt(digitalPinToInterrupt(BUTTON_INTERRUPT_1), interruptFunc_1, CHANGE); // Az utolsó Változó három értéket vehet fel RISING, FALLING CHANGE

    /*PWM*/
    ledcSetup(pwmChannel_1, frequency_1, resolution); // Konfigurációja a PWM csatornának
    ledcAttachPin(LED_PWM_1, pwmChannel_1);           // Rákötni egy Pint az adott pwm csatornára
}

void loop() {      // Végtelen ciklus, ide kerül a program logikánk.
/*
    Igazából ez valahogy így néz ki a main() függvényben:
    main() {
        initHardwareStuff(); // Ez inicializálja az órajelet, és egy csomó hardware függő dolgot
        setup();             // meghívja a fent említett setuppot
        loop();              // Ez igazából egy:   while(1) végtelen ciklus. 
    }
*/

    // Nézzük amiket tanultunk

    // KIMENETEK
    digitalWrite(LED_PIN_1, HIGH); // A LED_1-en definiált pin lábát felhúzza 3.3 Voltra.
    digitalWrite(LED_PIN_1, LOW); // A LED_1-en definiált pin lábát lehúzza Groundra
    ledcWrite(pwmChannel_1, 122); // Az adott csatornán lévő PWM jel kitöltésitényezőjének beállítása.
    
    // BEMENETEK
    
    // Sima Digital read
    int button1State = digitalRead(BUTTON_1); // integer értékkel visszatérő függvény, (HIGH vagy LOW előre definiált értékek.)
    if(button1State == HIGH) {
        // Ha a pin értéke elérte a HIGH állapothoz szükséges feszültséget ~kb 1.5 volt
    } else if (button1State == LOW) {
        // Ha a pin nem érte el a HIGH állapothoz szükséges feszültséget ~kb 1.5 Volt
    } else {
        // Valami félrement
    }

    // Analóg beolvasás
    analogValue1 = analogRead(POTMETER_1); // 0 - 4095 ig terjedő szám, 0 = GND = 0 Volt, 4095 = Vcc esetünkben 3.3Volt


}








