#include <Arduino.h>

#define LED_PIN 2
#define POT_PIN 15

uint16_t potVal       = 0;
uint8_t PwmChannel    = 0;      // 0 - 15
uint32_t PwmFrequency = 5000;   
uint8_t PwmResolution = 8;      //Resolution in bits 1 to 16


void setup() {
  ledcSetup(PwmChannel, PwmFrequency, PwmResolution);
  ledcAttachPin(LED_PIN, PwmChannel);
}

void loop() {
  for(int dutyCycle = 0; dutyCycle <= 255; dutyCycle++){   
    ledcWrite(PwmChannel, dutyCycle);
    delay(1);
  }
  for(int dutyCycle = 255; dutyCycle >= 0; dutyCycle--){   
    ledcWrite(PwmChannel, dutyCycle);
    delay(1);
  }
}
