#include <Arduino.h>

#define LED_1 2
#define LED_2 4
#define BUTTON_1 13
#define BUTTON_2 14

void interruptFunc_1();
void interruptFunc_2();

struct myButton
{
  uint counter = 0;
  bool state = false;
  uint8_t pinNumber = 0;
};

myButton Bt1;
myButton Bt2;
uint timecounter = 0;

void setup() {
  Bt1.pinNumber = BUTTON_1;
  Bt2.pinNumber = BUTTON_2;

  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(Bt1.pinNumber, INPUT_PULLDOWN);
  pinMode(Bt2.pinNumber, INPUT_PULLDOWN);

  digitalWrite(LED_1, LOW);
  digitalWrite(LED_2, LOW);

  attachInterrupt(digitalPinToInterrupt(Bt1.pinNumber), interruptFunc_1, RISING);
  attachInterrupt(digitalPinToInterrupt(Bt2.pinNumber), interruptFunc_2, FALLING);

  Serial.begin(9600);
}

void loop() {
  delay(10);
}


void interruptFunc_1() {
  if(!Bt1.state) {
    digitalWrite(LED_1, HIGH);
    Bt1.state = true;
    Bt1.counter++;
    Serial.print("Bt1 Clicked ");
    Serial.print(Bt1.counter);
    Serial.print(" times\n");
  } else {
    digitalWrite(LED_1, LOW);
    Bt1.state = false;
  }
}

void interruptFunc_2() {
  if(!Bt2.state) {
    digitalWrite(LED_2, HIGH);
    Bt2.state = true;
    Bt2.counter++;
    Serial.print("Bt2 Clicked ");
    Serial.print(Bt2.counter);
    Serial.print(" times\n");
  } else {
    digitalWrite(LED_2, LOW);
    Bt2.state = false;
  }
}