#include <Arduino.h>

#define LED_1 2
#define LED_2 4
#define BUTTON_1 13
#define BUTTON_2 14
#define POTMETER_1 15

void setup() {
  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(BUTTON_1, INPUT);
  pinMode(BUTTON_1, INPUT);

  digitalWrite(LED_1, LOW);
  digitalWrite(LED_2, LOW);

  Serial.begin(9600);
}

void loop() {

  if(digitalRead(BUTTON_1) == HIGH) {
    digitalWrite(LED_1, HIGH);
    Serial.write("BT1 CLICKED");
  } else {
    digitalWrite(LED_1, LOW);
  }

  if(digitalRead(BUTTON_2) == HIGH) {
    digitalWrite(LED_2, HIGH);
  } else {
    digitalWrite(LED_2, LOW);
  }

  delay(1);
}