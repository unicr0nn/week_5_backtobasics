#include <Arduino.h>

#define potPin 15
// variable for storing the potentiometer value
int potValue = 0;

double calcVoltage(int potval)
{
  return potval / 4095 * 3.3;
}


void setup() {
  Serial.begin(9600);
  delay(1000);
}

void loop() {
  // Reading potentiometer value
  potValue = analogRead(potPin);
  Serial.write("Pot value: ");
  Serial.println(potValue);
  Serial.write("Voltage: ");
  Serial.println(calcVoltage(potValue));
  delay(500);
}