#include <Arduino.h>

#define LED_1 2
#define LED_2 4
#define BUTTON_1 13
#define BUTTON_2 14

void interruptFunc_1();

void setup() {
  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(BUTTON_1, INPUT);
  pinMode(BUTTON_1, INPUT);

  digitalWrite(LED_1, LOW);
  digitalWrite(LED_2, LOW);

  attachInterrupt(digitalPinToInterrupt(BUTTON_1), interruptFunc_1, CHANGE);
}

void interruptFunc_1() {
  if(digitalRead(BUTTON_1) == HIGH) {
    digitalWrite(LED_1, HIGH);
  } else {
    digitalWrite(LED_1, LOW);
  }
}

void loop() {
  digitalWrite(LED_2, HIGH);
  delay(250); 
  digitalWrite(LED_2, LOW);
  delay(250);
}